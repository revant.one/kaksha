import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';
import { HomePage } from './home.page';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ChartsModule } from 'ng2-charts';
import { RouterTestingModule } from '@angular/router/testing';
import { StorageService } from '../common/services/storage/storage.service';

import { AppService } from '../app.service';
import { of } from 'rxjs';
import { OAuthService } from 'angular-oauth2-oidc';

describe('HomePage', () => {
  let component: HomePage;
  let fixture: ComponentFixture<HomePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [HomePage],
      imports: [
        IonicModule.forRoot(),
        HttpClientTestingModule,
        ChartsModule,
        RouterTestingModule,
      ],
      providers: [
        {
          provide: AppService,
          useValue: {
            setInfoLocalStorage: (...args) => null,
            getUserProfile: () => of({}),
            generateRandomString: (...args) => 'randomString',
            loadProfile: () => of({}),
          },
        },
        {
          provide: StorageService,
          useValue: {
            getItem: (...args) => Promise.resolve('ITEM'),
          },
        },
        {
          provide: OAuthService,
          useValue: {
            authorizationHeader: () => `Bearer token`,
          },
        },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(HomePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
