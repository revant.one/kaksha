import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ImportantNotePage } from './important-note.page';

const routes: Routes = [
  {
    path: '',
    component: ImportantNotePage,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ImportantNotePageRoutingModule {}
