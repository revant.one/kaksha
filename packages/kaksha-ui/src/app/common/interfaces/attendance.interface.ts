export interface Attendance {
  program: string;
  course_name: string;
  attendancesList?: StudentAttendance[];
}
export interface StudentAttendance {
  date: string;
  rollno: string;
  name: string;
  remarks: string;
}
