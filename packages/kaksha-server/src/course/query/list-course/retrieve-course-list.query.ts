import { IQuery } from '@nestjs/cqrs';

export class RetrieveCourseListQuery implements IQuery {
  constructor(
    public offset: number,
    public limit: number,
    public sort: number,
    public search: string,
    public clientHttpRequest: any,
  ) {}
}
