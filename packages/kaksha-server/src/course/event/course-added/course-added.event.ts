import { IEvent } from '@nestjs/cqrs';
import { Course } from '../../entity/course/course.entity';

export class CourseAddedEvent implements IEvent {
  constructor(public course: Course, public clientHttpRequest: any) {}
}
