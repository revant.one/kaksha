import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { CourseAddedEvent } from './course-added.event';
import { CourseService } from '../../entity/course/course.service';

@EventsHandler(CourseAddedEvent)
export class CourseAddedEventHandler
  implements IEventHandler<CourseAddedEvent> {
  constructor(private readonly courseService: CourseService) {}
  async handle(event: CourseAddedEvent) {
    const { course } = event;
    await this.courseService.create(course);
  }
}
