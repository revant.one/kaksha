import { InjectRepository } from '@nestjs/typeorm';
import { Course } from './course.entity';
import { Injectable } from '@nestjs/common';
import { MongoRepository } from 'typeorm';

@Injectable()
export class CourseService {
  constructor(
    @InjectRepository(Course)
    private readonly courseRepository: MongoRepository<Course>,
  ) {}

  async find(query?) {
    return await this.courseRepository.find(query);
  }

  async create(course: Course) {
    const courseObject = new Course();
    Object.assign(courseObject, course);
    return await this.courseRepository.insertOne(courseObject);
  }

  async findOne(param, options?) {
    return await this.courseRepository.findOne(param, options);
  }

  async list(skip, take, search, sort) {
    const nameExp = new RegExp(search, 'i');
    const columns = this.courseRepository.manager.connection
      .getMetadata(Course)
      .ownColumns.map(column => column.propertyName);

    const $or = columns.map(field => {
      const filter = {};
      filter[field] = nameExp;
      return filter;
    });
    const $and: any[] = [{ $or }];

    const where: { $and: any } = { $and };

    const results = await this.courseRepository.find({
      skip,
      take,
      where,
    });

    return {
      docs: results || [],
      length: await this.courseRepository.count(where),
      offset: skip,
    };
  }

  async deleteOne(query, options?) {
    return await this.courseRepository.deleteOne(query, options);
  }

  async updateOne(query, options?) {
    return await this.courseRepository.updateOne(query, options);
  }
}
