import { IEvent } from '@nestjs/cqrs';
import { Topic } from '../../entity/topic/topic.entity';

export class TopicUpdatedEvent implements IEvent {
  constructor(public updatePayload: Topic) {}
}
