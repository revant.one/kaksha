import { Column, ObjectIdColumn, BaseEntity, ObjectID, Entity } from 'typeorm';

@Entity()
export class Attendance extends BaseEntity {
  @ObjectIdColumn()
  _id: ObjectID;

  @Column()
  uuid: string;

  @Column()
  name: string;

  @Column()
  docstatus: number;

  @Column()
  student: string;

  @Column()
  course_schedule: string;

  @Column()
  date: string;

  @Column()
  student_name: string;

  @Column()
  student_group: string;

  @Column()
  status: string;

  @Column()
  doctype: string;

  @Column()
  isSynced: boolean;
}
