import { IQuery } from '@nestjs/cqrs';

export class RetrieveProgramQuery implements IQuery {
  constructor(public readonly uuid: string, public readonly req: any) {}
}
