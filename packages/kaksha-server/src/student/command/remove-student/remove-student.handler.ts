import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { RemoveStudentCommand } from './remove-student.command';
import { StudentAggregateService } from '../../aggregates/student-aggregate/student-aggregate.service';

@CommandHandler(RemoveStudentCommand)
export class RemoveStudentCommandHandler
  implements ICommandHandler<RemoveStudentCommand> {
  constructor(
    private readonly publisher: EventPublisher,
    private readonly manager: StudentAggregateService,
  ) {}
  async execute(command: RemoveStudentCommand) {
    const { uuid } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await aggregate.remove(uuid);
    aggregate.commit();
  }
}
