import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { RetrieveStudentQuery } from './retrieve-student.query';
import { StudentAggregateService } from '../../aggregates/student-aggregate/student-aggregate.service';

@QueryHandler(RetrieveStudentQuery)
export class RetrieveStudentQueryHandler
  implements IQueryHandler<RetrieveStudentQuery> {
  constructor(private readonly manager: StudentAggregateService) {}

  async execute(query: RetrieveStudentQuery) {
    const { req, uuid } = query;
    return this.manager.retrieveStudent(uuid, req);
  }
}
