import { IEvent } from '@nestjs/cqrs';
import { Article } from '../../entity/article/article.entity';

export class ArticleUpdatedEvent implements IEvent {
  constructor(public updatePayload: Article) {}
}
