import { ICommand } from '@nestjs/cqrs';
import { ArticleDto } from '../../entity/article/article-dto';

export class AddArticleCommand implements ICommand {
  constructor(
    public articlePayload: ArticleDto,
    public readonly clientHttpRequest: any,
  ) {}
}
