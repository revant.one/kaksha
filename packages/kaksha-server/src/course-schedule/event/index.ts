import { CourseScheduleAddedCommandHandler } from './course-schedule-added/course-schedule-added.handler';
import { CourseScheduleRemovedCommandHandler } from './course-schedule-removed/course-schedule-removed.handler';
import { CourseScheduleUpdatedCommandHandler } from './course-schedule-updated/course-schedule-updated.handler';

export const CourseScheduleEventManager = [
  CourseScheduleAddedCommandHandler,
  CourseScheduleRemovedCommandHandler,
  CourseScheduleUpdatedCommandHandler,
];
