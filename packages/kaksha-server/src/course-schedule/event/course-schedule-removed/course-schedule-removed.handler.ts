import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { CourseScheduleService } from '../../entity/course-schedule/course-schedule.service';
import { CourseScheduleRemovedEvent } from './course-schedule-removed.event';

@EventsHandler(CourseScheduleRemovedEvent)
export class CourseScheduleRemovedCommandHandler
  implements IEventHandler<CourseScheduleRemovedEvent> {
  constructor(private readonly courseScheduleService: CourseScheduleService) {}
  async handle(event: CourseScheduleRemovedEvent) {
    const { courseSchedule } = event;
    await this.courseScheduleService.deleteOne({ uuid: courseSchedule.uuid });
  }
}
