import { Injectable, NotFoundException } from '@nestjs/common';
import { AggregateRoot } from '@nestjs/cqrs';
import * as uuidv4 from 'uuid/v4';
import { CourseScheduleDto } from '../../entity/course-schedule/course-schedule-dto';
import { CourseSchedule } from '../../entity/course-schedule/course-schedule.entity';
import { CourseScheduleAddedEvent } from '../../event/course-schedule-added/course-schedule-added.event';
import { CourseScheduleService } from '../../entity/course-schedule/course-schedule.service';
import { CourseScheduleRemovedEvent } from '../../event/course-schedule-removed/course-schedule-removed.event';
import { CourseScheduleUpdatedEvent } from '../../event/course-schedule-updated/course-schedule-updated.event';
import { UpdateCourseScheduleDto } from '../../entity/course-schedule/update-course-schedule-dto';

@Injectable()
export class CourseScheduleAggregateService extends AggregateRoot {
  constructor(private readonly courseScheduleService: CourseScheduleService) {
    super();
  }

  addCourseSchedule(
    courseSchedulePayload: CourseScheduleDto,
    clientHttpRequest,
  ) {
    const courseSchedule = new CourseSchedule();
    Object.assign(courseSchedule, courseSchedulePayload);
    courseSchedule.uuid = uuidv4();
    this.apply(new CourseScheduleAddedEvent(courseSchedule, clientHttpRequest));
  }

  async retrieveCourseSchedule(uuid: string, req) {
    const provider = await this.courseScheduleService.findOne({ uuid });
    if (!provider) throw new NotFoundException();
    return provider;
  }

  async getCourseScheduleList(offset, limit, sort, search, clientHttpRequest) {
    return await this.courseScheduleService.list(offset, limit, search, sort);
  }

  async remove(uuid: string) {
    const found = await this.courseScheduleService.findOne({ uuid });
    if (!found) {
      throw new NotFoundException();
    }
    this.apply(new CourseScheduleRemovedEvent(found));
  }

  async update(updatePayload: UpdateCourseScheduleDto) {
    const provider = await this.courseScheduleService.findOne({
      uuid: updatePayload.uuid,
    });
    if (!provider) {
      throw new NotFoundException();
    }
    const update = Object.assign(provider, updatePayload);
    this.apply(new CourseScheduleUpdatedEvent(update));
  }
}
