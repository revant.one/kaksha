import { IsNotEmpty, IsOptional, IsString, IsNumber } from 'class-validator';
export class UpdateCourseScheduleDto {
  @IsNotEmpty()
  uuid: string;

  @IsOptional()
  @IsString()
  name: string;

  @IsOptional()
  @IsNumber()
  docstatus: number;

  @IsOptional()
  @IsString()
  student_group: string;

  @IsOptional()
  @IsString()
  instructor: string;

  @IsOptional()
  @IsString()
  instructor_name: string;

  @IsOptional()
  @IsString()
  naming_series: string;

  @IsOptional()
  @IsString()
  course: string;

  @IsOptional()
  @IsString()
  schedule_date: string;

  @IsOptional()
  @IsString()
  room: string;

  @IsOptional()
  @IsString()
  from_time: string;

  @IsOptional()
  @IsString()
  to_time: string;

  @IsOptional()
  @IsString()
  title: string;

  @IsOptional()
  @IsString()
  doctype: string;
}
