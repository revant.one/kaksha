import { InjectRepository } from '@nestjs/typeorm';
import { CourseSchedule } from './course-schedule.entity';
import { Injectable } from '@nestjs/common';
import { MongoRepository } from 'typeorm';
import { of } from 'rxjs';
import { switchMap } from 'rxjs/operators';

@Injectable()
export class CourseScheduleService {
  constructor(
    @InjectRepository(CourseSchedule)
    private readonly courseScheduleRepository: MongoRepository<CourseSchedule>,
  ) {}

  async find(query?) {
    return await this.courseScheduleRepository.find(query);
  }

  async create(courseSchedule: CourseSchedule) {
    const courseScheduleObject = new CourseSchedule();
    Object.assign(courseScheduleObject, courseSchedule);
    return await this.courseScheduleRepository.insertOne(courseScheduleObject);
  }

  async findOne(param, options?) {
    return await this.courseScheduleRepository.findOne(param, options);
  }

  async list(skip, take, search, sort) {
    const nameExp = new RegExp(search, 'i');
    const columns = this.courseScheduleRepository.manager.connection
      .getMetadata(CourseSchedule)
      .ownColumns.map(column => column.propertyName);

    const $or = columns.map(field => {
      const filter = {};
      filter[field] = nameExp;
      return filter;
    });
    const $and: any[] = [{ $or }];

    const where: { $and: any } = { $and };

    const results = await this.courseScheduleRepository.find({
      skip,
      take,
      where,
    });

    return {
      docs: results || [],
      length: await this.courseScheduleRepository.count(where),
      offset: skip,
    };
  }

  async deleteOne(query, options?) {
    return await this.courseScheduleRepository.deleteOne(query, options);
  }

  async updateOne(query, options?) {
    return await this.courseScheduleRepository.updateOne(query, options);
  }
  asyncAggregate(query) {
    return of(this.courseScheduleRepository.aggregate(query)).pipe(
      switchMap((aggregateData: any) => {
        return aggregateData.toArray();
      }),
    );
  }
}
