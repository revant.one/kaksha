import { Test, TestingModule } from '@nestjs/testing';
import { CourseSchedulePoliciesService } from './course-schedule-policies.service';

describe('CourseSchedulePoliciesService', () => {
  let service: CourseSchedulePoliciesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [CourseSchedulePoliciesService],
    }).compile();

    service = module.get<CourseSchedulePoliciesService>(
      CourseSchedulePoliciesService,
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
