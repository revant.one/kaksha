import { Test, TestingModule } from '@nestjs/testing';
import { TeacherAggregateService } from './teacher-aggregate.service';
import { ServerSettings } from '../../../system-settings/entities/server-settings/server-settings.entity';
import { HttpService } from '@nestjs/common';
import { RoomService } from '../../../room/entity/room/room.service';
describe('teacherAggregateService', () => {
  let service: TeacherAggregateService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        TeacherAggregateService,
        {
          provide: TeacherAggregateService,
          useValue: {},
        },
        {
          provide: ServerSettings,
          useValue: {},
        },
        {
          provide: HttpService,
          useValue: {},
        },
        {
          provide: RoomService,
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<TeacherAggregateService>(TeacherAggregateService);
  });
  TeacherAggregateService;
  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
