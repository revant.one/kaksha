import { IsString, IsNotEmpty } from 'class-validator';

export class ScheduleDateDto {
  @IsString()
  @IsNotEmpty()
  from_date: string;

  @IsString()
  @IsNotEmpty()
  to_date: string;
}
