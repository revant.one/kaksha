import {
  IsNotEmpty,
  IsString,
  IsOptional,
  IsNumber,
  ValidateNested,
} from 'class-validator';
import { Type } from 'class-transformer';

export class UpdateTeacherDto {
  @IsNotEmpty()
  uuid: string;

  @IsString()
  @IsOptional()
  name: string;

  @IsNumber()
  @IsOptional()
  docstatus: number;

  @IsString()
  @IsOptional()
  instructor_name: string;

  @IsString()
  @IsOptional()
  employee: string;

  @IsString()
  @IsOptional()
  doctype: string;

  @ValidateNested()
  @Type(() => UpdateTeacherLogDto)
  instructor_log: UpdateTeacherLogDto[];
}
export class UpdateTeacherLogDto {
  @IsString()
  @IsOptional()
  name: string;

  @IsNumber()
  @IsOptional()
  docstatus: number;

  @IsString()
  @IsOptional()
  academic_year: string;

  @IsString()
  @IsOptional()
  academic_term: string;

  @IsString()
  @IsOptional()
  program: string;

  @IsString()
  @IsOptional()
  course: string;

  @IsString()
  @IsOptional()
  doctype: string;
}
