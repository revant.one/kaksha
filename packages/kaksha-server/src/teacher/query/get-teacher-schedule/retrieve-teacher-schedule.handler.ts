import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { RetrieveTeacherScheduleQuery } from './retrieve-teacher-schedule.query';
import { TeacherAggregateService } from '../../aggregates/teacher-aggregate/teacher-aggregate.service';

@QueryHandler(RetrieveTeacherScheduleQuery)
export class RetrieveTeacherSchduleQueryHandler
  implements IQueryHandler<RetrieveTeacherScheduleQuery> {
  constructor(private readonly manager: TeacherAggregateService) {}

  async execute(query: RetrieveTeacherScheduleQuery) {
    const { limit, offset, search, sort, datePayload, req } = query;
    return this.manager
      .getTeacherSchedule(
        Number(limit),
        Number(offset),
        search,
        sort,
        datePayload,
        req,
      )
      .toPromise();
  }
}
