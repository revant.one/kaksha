import { Test, TestingModule } from '@nestjs/testing';
import { QuestionAggregateService } from './question-aggregate.service';
import { QuestionService } from '../../entity/question/question.service';
import { HttpService } from '@nestjs/common';
import { SettingsService } from '../../../system-settings/aggregates/settings/settings.service';

describe('QuestionAggregateService', () => {
  let service: QuestionAggregateService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        QuestionAggregateService,
        {
          provide: QuestionService,
          useValue: {},
        },
        {
          provide: SettingsService,
          useValue: {},
        },
        {
          provide: HttpService,
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<QuestionAggregateService>(QuestionAggregateService);
  });
  QuestionAggregateService;
  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
