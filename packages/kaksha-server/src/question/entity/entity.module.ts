import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Question } from './question/question.entity';
import { QuestionService } from './question/question.service';
import { CqrsModule } from '@nestjs/cqrs';
import { QuizEntitiesModule } from '../../quiz/entity/entity.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([Question]),
    CqrsModule,
    QuizEntitiesModule,
  ],
  providers: [QuestionService],
  exports: [QuestionService],
})
export class QuestionEntitiesModule {}
