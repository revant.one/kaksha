import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { StudentGroup } from './student-group/student-group.entity';
import { StudentGroupService } from './student-group/student-group.service';
import { CqrsModule } from '@nestjs/cqrs';

@Module({
  imports: [TypeOrmModule.forFeature([StudentGroup]), CqrsModule],
  providers: [StudentGroupService],
  exports: [StudentGroupService],
})
export class StudentGroupEntitiesModule {}
