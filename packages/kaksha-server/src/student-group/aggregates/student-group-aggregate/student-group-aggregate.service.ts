import { Injectable, NotFoundException } from '@nestjs/common';
import { AggregateRoot } from '@nestjs/cqrs';
import * as uuidv4 from 'uuid/v4';
import { StudentGroupDto } from '../../entity/student-group/student-group-dto';
import { StudentGroup } from '../../entity/student-group/student-group.entity';
import { StudentGroupAddedEvent } from '../../event/student-group-added/student-group-added.event';
import { StudentGroupService } from '../../entity/student-group/student-group.service';
import { StudentGroupRemovedEvent } from '../../event/student-group-removed/student-group-removed.event';
import { StudentGroupUpdatedEvent } from '../../event/student-group-updated/student-group-updated.event';
import { UpdateStudentGroupDto } from '../../entity/student-group/update-student-group-dto';

@Injectable()
export class StudentGroupAggregateService extends AggregateRoot {
  constructor(private readonly studentGroupService: StudentGroupService) {
    super();
  }

  addStudentGroup(studentGroupPayload: StudentGroupDto, clientHttpRequest) {
    const studentGroup = new StudentGroup();
    Object.assign(studentGroup, studentGroupPayload);
    studentGroup.uuid = uuidv4();
    this.apply(new StudentGroupAddedEvent(studentGroup, clientHttpRequest));
  }

  async retrieveStudentGroup(uuid: string, req) {
    const provider = await this.studentGroupService.findOne({ uuid });
    if (!provider) throw new NotFoundException();
    return provider;
  }

  async getStudentGroupList(offset, limit, sort, search, clientHttpRequest) {
    return await this.studentGroupService.list(offset, limit, search, sort);
  }

  async remove(uuid: string) {
    const found = await this.studentGroupService.findOne({ uuid });
    if (!found) {
      throw new NotFoundException();
    }
    this.apply(new StudentGroupRemovedEvent(found));
  }

  async update(updatePayload: UpdateStudentGroupDto) {
    const provider = await this.studentGroupService.findOne({
      uuid: updatePayload.uuid,
    });
    if (!provider) {
      throw new NotFoundException();
    }
    const update = Object.assign(provider, updatePayload);
    this.apply(new StudentGroupUpdatedEvent(update));
  }
}
