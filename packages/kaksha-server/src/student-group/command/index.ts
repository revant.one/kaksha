import { AddStudentGroupCommandHandler } from './add-student-group/add-student-group.handler';
import { RemoveStudentGroupCommandHandler } from './remove-student-group/remove-student-group.handler';
import { UpdateStudentGroupCommandHandler } from './update-student-group/update-student-group.handler';

export const StudentGroupCommandManager = [
  AddStudentGroupCommandHandler,
  RemoveStudentGroupCommandHandler,
  UpdateStudentGroupCommandHandler,
];
