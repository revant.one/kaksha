import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { RetrieveQuizQuery } from './retrieve-quiz.query';
import { QuizAggregateService } from '../../aggregates/quiz-aggregate/quiz-aggregate.service';

@QueryHandler(RetrieveQuizQuery)
export class RetrieveQuizQueryHandler
  implements IQueryHandler<RetrieveQuizQuery> {
  constructor(private readonly manager: QuizAggregateService) {}

  async execute(query: RetrieveQuizQuery) {
    const { offset, limit, sort, search, req, getQuizPayload } = query;
    return await this.manager
      .retrieveQuiz(
        Number(offset),
        Number(limit),
        sort,
        search,
        getQuizPayload,
        req,
      )
      .toPromise();
  }
}
