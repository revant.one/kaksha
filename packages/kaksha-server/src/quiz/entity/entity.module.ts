import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Quiz } from './quiz/quiz.entity';
import { QuizService } from './quiz/quiz.service';
import { CqrsModule } from '@nestjs/cqrs';
import { TopicEntitiesModule } from '../../topic/entity/entity.module';

@Module({
  imports: [TypeOrmModule.forFeature([Quiz]), CqrsModule, TopicEntitiesModule],
  providers: [QuizService],
  exports: [QuizService],
})
export class QuizEntitiesModule {}
