import { IEvent } from '@nestjs/cqrs';
import { Quiz } from '../../entity/quiz/quiz.entity';

export class QuizUpdatedEvent implements IEvent {
  constructor(public updatePayload: Quiz) {}
}
