import { RoomAddedCommandHandler } from './room-added/room-added.handler';
import { RoomRemovedCommandHandler } from './room-removed/room-removed.handler';
import { RoomUpdatedCommandHandler } from './room-updated/room-updated.handler';

export const RoomEventManager = [
  RoomAddedCommandHandler,
  RoomRemovedCommandHandler,
  RoomUpdatedCommandHandler,
];
