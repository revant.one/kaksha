import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { RetrieveRoomQuery } from './retrieve-room.query';
import { RoomAggregateService } from '../../aggregates/room-aggregate/room-aggregate.service';

@QueryHandler(RetrieveRoomQuery)
export class RetrieveRoomQueryHandler
  implements IQueryHandler<RetrieveRoomQuery> {
  constructor(private readonly manager: RoomAggregateService) {}

  async execute(query: RetrieveRoomQuery) {
    const { req, uuid } = query;
    return this.manager.retrieveRoom(uuid, req);
  }
}
