import { ICommand } from '@nestjs/cqrs';

export class RemoveVideoCommand implements ICommand {
  constructor(public readonly uuid: string) {}
}
