import { IEvent } from '@nestjs/cqrs';
import { Video } from '../../entity/video/video.entity';

export class VideoAddedEvent implements IEvent {
  constructor(public video: Video, public clientHttpRequest: any) {}
}
